Logs for each AWS instances are located in `logs` directory marked by `aws-<instance_location>-<operation>-<destination>.txt`  
i.e. ping log to pantip.com from an instance in Sydney would be name as `aws-sydney-ping-pantip.txt`

------

To run the program, public ssh key must be added on the test instance from the aws site.  
Edit the main class with the ssh login for each instances.  

make sure Gradle build tool is installed. To compile `gradle build` and jar file is located in `build/libs`

This program send commands to each instances simultaneously, but each instances execute tasks synchronisely in order to allocated network traffic for a specific tasks only and for the outcome to be accurate without any interferences.
