package io.muic.nodefinder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class Main {
  public static void main(String[] args) throws ExecutionException, InterruptedException {
    List<Map<String, String>> lst = new ArrayList<>();

//    System.out.println("Blah");
    lst.add(new HashMap<String, String>(){{
      put("location", "Singapore");
      put("user", "ec2-user@ec2-54-255-214-164.ap-southeast-1.compute.amazonaws.com");
    }});

    lst.add(new HashMap<String, String>(){{
      put("location", "Sydney");
      put("user", "ec2-user@ec2-54-206-118-243.ap-southeast-2.compute.amazonaws.com");
    }});

    lst.add(new HashMap<String, String>(){{
      put("location", "Seoul");
      put("user", "ec2-user@ec2-13-124-30-169.ap-northeast-2.compute.amazonaws.com");
    }});

    lst.add(new HashMap<String, String>(){{
      put("location", "Tokyo");
      put("user", "ec2-user@ec2-54-250-169-29.ap-northeast-1.compute.amazonaws.com");
    }});

    lst.add(new HashMap<String, String>(){{
      put("location", "Mumbai");
      put("user", "ec2-user@ec2-35-154-154-99.ap-south-1.compute.amazonaws.com");
    }});

//    System.out.println("Blah");
    FileWriter fileWriter = new FileWriter();
    ExecutorService pool = Executors.newFixedThreadPool(lst.size());
    List<Future<Boolean>> poolList = new ArrayList<>();
    long start = System.nanoTime();
    for (Map<String,String> map : lst){
      poolList.add(pool.submit(new ProcessRunner(map.get("location"), map.get("user"), fileWriter)));
    }

    for (Future<Boolean> l : poolList){
      l.get();
    }

    long finish = System.nanoTime();
    System.out.println("Time Taken: " + TimeUnit.NANOSECONDS.toMinutes(finish - start));
    pool.shutdown();
  }
}
