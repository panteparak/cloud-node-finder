package io.muic.nodefinder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Callable;

public class ProcessRunner implements Callable<Boolean>{
  private String login;
  private FileWriter fileWriter;
  private String location;

  public ProcessRunner(String location, String login, FileWriter fileWriter) {
    this.login = login;
    this.fileWriter = fileWriter;
    this.location = location;
  }

  @Override
  public Boolean call() throws Exception {

    this.fileWriter.write(location, Command.TraceGoogle, cmdRunner(Command.TraceGoogle));
    this.fileWriter.write(location, Command.PingGoogle, cmdRunner(Command.PingGoogle));
    this.fileWriter.write(location, Command.TracePantip, cmdRunner(Command.TracePantip));
    this.fileWriter.write(location, Command.PingPantip, cmdRunner(Command.PingPantip));

    System.out.println("LOCATION: " + location + " DONE!");
    return true;

  }

  private ResultOutput cmdRunner(Command command){
    try {
      System.out.println("LOCATION: " + location + " " + command.getName());
      String[] cmd = {"ssh", "-o", "StrictHostKeyChecking=no",login, command.toString()};
      Process process = Runtime.getRuntime().exec(cmd);
      BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));

      String line;
      StringBuilder stringBuilder = new StringBuilder();

      while ((line = br.readLine()) != null){
        stringBuilder.append(line.trim() + "\n");
      }
      ResultOutput resultOutput = new ResultOutput();
      resultOutput.setName(command.getName());
      resultOutput.setData(stringBuilder.toString());
      resultOutput.setExitCode(process.waitFor());
      return resultOutput;

    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    return null;

  }
}
