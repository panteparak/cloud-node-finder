package io.muic.nodefinder;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.file.Paths;

public class FileWriter {
  private String jarDir;

  public FileWriter() {
    String saveFolder;
    try {
      String url = Main.class.getProtectionDomain().getCodeSource().getLocation().getFile();
      String decode = URLDecoder.decode(url, "UTF-8");
      saveFolder = new File(decode).getParentFile().getPath();
      System.out.println(saveFolder);
      jarDir = saveFolder;
    } catch (IOException e){
      e.printStackTrace();
    }

  }

  public void write(String location, Command cmd, ResultOutput output){
    String filename = "aws-" + location.toLowerCase() + "-" + cmd.getName() + ".txt";
    File file = Paths.get(jarDir, filename).toFile();
    System.out.println("Write to: " + file.getAbsolutePath());
    System.out.println("exit code: " + output.getExitCode());
    try {
      FileUtils.writeStringToFile(file, output.getData(), Charset.defaultCharset());

    } catch (IOException e){
      e.printStackTrace();
    }


  }
}
