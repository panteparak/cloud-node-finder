package io.muic.nodefinder;

public class ResultOutput {
  private String name;
  private String data;
  private int exitCode;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public int getExitCode() {
    return exitCode;
  }

  public void setExitCode(int exitCode) {
    this.exitCode = exitCode;
  }

  @Override
  public String toString() {
    return "ResultOutput{" +
            "name='" + name + '\'' +
            ", data='" + data + '\'' +
            ", exitCode=" + exitCode +
            '}';
  }
}
