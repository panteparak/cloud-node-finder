package io.muic.nodefinder;

/**
 * Created by panteparak on 4/25/17.
 */
public enum Command {
  TraceGoogle("traceroute google.com", "traceroute-google"), TracePantip("traceroute pantip.com", "traceroute-pantip"),
  PingGoogle("ping -c 100 google.com", "ping-google"), PingPantip("ping -c 100 pantip.com", "ping-pantip");

  private String cmd;
  private String name;

  Command(String cmd, String name) {
    this.cmd = cmd;
    this.name = name;
  }

  @Override
  public String toString() {
    return this.cmd;
  }

  public String getName() {
    return name;
  }
}
